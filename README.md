Om het project 'lokaal' dus op je eigen computer te draaien
download je het project of gebruik je de terminal of commandpromt cmd

git clone https://gitlab.com/thecoderebels/wagtail_gamecoders.git

dan cd in de map en 
in je terminal 
(zonder $ dat betekent dat het een linux commando is)
$ cd wagtail_gamecoders

Maak nu eerst de virtualenv aan
$ python3 -m venv gamecoders

En activeer deze: 
$ source gamecoders/bin/activate

#je command promt heeft nu iets tussen haakjes (gamecoders

Installeer wagtailen django met
$ pip install -r requirements.txt 

$ python manage.py runserver

error? Even ls doen om te kijken of je manage.py ook in de directory zit.
Anders cd.. voor directory hoger
en cd naam_map voor map hoger

#je command promt heeft nu iets tussen haakjes

om het project te draaien 
doe je 
$ls 
in de map staat nu als het goed is een manage.py bestand.

de instructie staat op bit.ly/rebeltail
